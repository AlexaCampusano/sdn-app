# SDN App 

Aplicación web para ser la interfaz gráfica de una aplicación SDN para la gestión de VLANS.

# ¿Qué puedes hacer con esta web app?

  - Crear, Eliminar y Visualizar VLANs
  - Asignar Roles a Switches existentes
  - Ver un historial de las acciones realizadas en la aplicación

### Tech

SDN App utiliza una serie de aplicaciones open source para trabajar de manera efectiva

* [Angular] - HTML a otro nivel para manejar la interacción de la aplicación con el usuario
* [Angular Bootstrap] - Ayuda con el estilo y todo el look-nd-feel de la aplicación
* [Node.js] - JavaScript en el backend. Utilizamos este para llevar acabo el logging de la aplicación
* [Express] - fast node.js network app framework [@tjholowaychuk]

### Installation

SDN App necesita [Node.js](https://nodejs.org/) v10+ para correr.
Luego de instalar [Node.js](https://nodejs.org/), instalar las dependencias del proyecto para poder correr la aplicación.

```sh
$ cd sdn-app
$ npm install
$ npm start
```

### Estructura del proyecto

* Mockups folder: Este contiene imágenes del diseño inicial de la aplicación.
* sdn-app folder: Este contiene todo el contenido del desarrollo de la aplicación web. Realizado en [Angular].
* README.md: Este archivo contiene documentación sobre qué es SDN App, como correr el proyecto, y las tecnologías utilizadas en el proyecto.

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


 
   [node.js]: <http://nodejs.org>
   [Angular Bootstrap]: <https://ng-bootstrap.github.io>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [Angular]: <https://angular.io>
