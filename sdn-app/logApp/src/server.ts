import * as http from 'http';
import * as fs from 'fs';
import express from "express";
import * as bodyParser from "body-parser";

const port = 3000;
const app = express();
let log = [];

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, \Content-Type, Authorization, Accept');
    next();
});

app.post('/log', function (req, res) {
    log.push(req.body.logInfo);
    fs.writeFile('logFile.json', JSON.stringify(log), (error: Error) => {
        if (error) {
            throw error;
        }
        res.send();
    });
});

app.get('/log', function (req, res) {

    fs.readFile('logFile.json', { encoding: 'UTF8' }, (error: Error, data: string) => {
        if (error) {
            throw error;
        }
        res.send({ data: data });
    });
});

app.listen(port, () => {
    console.log('Listening through Port ' + port);
});
