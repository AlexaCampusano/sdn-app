import { Routes } from '@angular/router';
import { RolesComponent } from './app/roles/roles.component';
import { VlansComponent } from './app/vlans/vlans.component';
import { LogsComponent } from './app/logs/logs.component';

export const appRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'roles',
                component: RolesComponent
            },
            {
                path: 'vlans',
                component: VlansComponent
            },
            {
                path: 'logs',
                component: LogsComponent
            }
        ]
    }
]