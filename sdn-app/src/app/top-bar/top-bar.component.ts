import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  toggle = true;
  constructor() { }

  ngOnInit() {
    localStorage.setItem('side-bar-open', this.toggle ? 'false': 'true');
  }

  toggleSideBar = () => {
    this.toggle = !this.toggle;
    localStorage.setItem('side-bar-open', this.toggle ? 'false': 'true');
    return this.toggle;
  }

}
