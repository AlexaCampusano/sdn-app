import { TestBed } from '@angular/core/testing';

import { VlansService } from './vlans.service';

describe('VlansService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VlansService = TestBed.get(VlansService);
    expect(service).toBeTruthy();
  });
});
