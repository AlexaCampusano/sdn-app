import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { flatMap, switchMap } from 'rxjs/operators';
import { LogsService } from './logs.service';
const baseUrl = `${environment.api}`;
@Injectable({
  providedIn: 'root'
})
export class VlansService {

  constructor(private http: HttpClient, private log: LogsService) { }

  getVlans = (): Observable<any> => {
    return this.http.get<any>(`${baseUrl}/vlan`);
  }

  createVlans = (data: any): Observable<any> => {
    this.log.info('Admin', 'Creating VLANs', JSON.stringify(data), `POST ${baseUrl}/vlan`);
    return this.http.post<any>(`${baseUrl}/vlan`, JSON.stringify(data));
  }

  deleteVlan = (vlanId: number): Observable<any> => {
    this.log.info('Admin', 'Deleting VLANs', JSON.stringify({vlan: vlanId}), `POST ${baseUrl}/vlan`);
    return this.http.post<any>(`${baseUrl}/delete`, JSON.stringify({vlan: parseInt(vlanId.toString())}));
  }

  updateVlans = (data: any): Observable<any> => {
    this.log.info('Admin', 'Updating VLAN', JSON.stringify(data), `POST ${baseUrl}/vlan`);
    return this.deleteVlan(data.vlan).pipe(switchMap((result: any) => this.createVlans(data)));
  }
}
