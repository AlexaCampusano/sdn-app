import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LogsService } from './logs.service';

const baseUrl = `${environment.api}`;

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient, private log: LogsService) { }

  getRoles = (): Observable<any> => {
    return this.http.get<any>(`${baseUrl}/roles`);
  }

  setRole = (data: any): Observable<any> => {
    this.log.info('Admin', 'Creating Roles', JSON.stringify(data), `POST ${baseUrl}/roles`);
    return this.http.post<any>(`${baseUrl}/roles`, JSON.stringify(data));
  }
}
