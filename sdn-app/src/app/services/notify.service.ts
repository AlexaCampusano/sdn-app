import { Injectable } from '@angular/core';
import { NotifyConfig } from '../_models/notify-config';
declare let VanillaToasts: any;
@Injectable({
  providedIn: 'root'
})
export class NotifyService {
  config: NotifyConfig;
  constructor() {}

  success = (message: string, callback: any = () => {}) => {
    this.config = new NotifyConfig('SUCCESS', message, 'success', '', callback);
    VanillaToasts.create(this.config);
  }

  error = (message: string, callback: any = () => {}) => {
    this.config = new NotifyConfig('ERROR', message, 'error', '', callback);
    VanillaToasts.create(this.config);
  }

  info = (message: string, callback: any = () => {}) => {
    this.config = new NotifyConfig('INFO', message, 'info', '', callback);
    VanillaToasts.create(this.config);
  }

  warning = (message: string, callback: any = () => {}) => {
    this.config = new NotifyConfig('WARNING', message, 'warning', '', callback);
    VanillaToasts.create(this.config);
  }
}
