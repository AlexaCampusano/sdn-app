import { Injectable } from '@angular/core';
import { Log } from '../_models/logs';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
const logService = environment.logService;

@Injectable({
  providedIn: 'root'
})
export class LogsService {
  constructor(private http: HttpClient) { }

  log = (createdBy: string, message: string, dataSent: string, apiRoute: string): Observable<any> => {
    let logInfo = new Log(createdBy, message, dataSent, apiRoute);
    return this.http.post(logService, {logInfo: logInfo});
  }

  info = (createdBy: string, message: string, dataSent: string, apiRoute: string) => {
    this.log(createdBy, message,  dataSent, apiRoute).subscribe(response => {});
  }

  getLogs = () => {
    return this.http.get(logService);
  }
}
