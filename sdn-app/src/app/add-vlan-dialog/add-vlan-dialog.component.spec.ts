import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVlanDialogComponent } from './add-vlan-dialog.component';

describe('AddVlanDialogComponent', () => {
  let component: AddVlanDialogComponent;
  let fixture: ComponentFixture<AddVlanDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVlanDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVlanDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
