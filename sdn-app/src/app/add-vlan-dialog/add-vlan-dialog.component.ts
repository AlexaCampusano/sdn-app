import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LogsService } from '../services/logs.service';
import { parse } from 'querystring';

@Component({
  selector: 'app-add-vlan-dialog',
  templateUrl: './add-vlan-dialog.component.html',
  styleUrls: ['./add-vlan-dialog.component.css']
})
export class AddVlanDialogComponent implements OnInit {
  @ViewChild('content', { static: false}) content: any;
  @Output() onClosed: EventEmitter<any> = new EventEmitter();
  @Output() onSaved: EventEmitter<any> = new EventEmitter();
  closeResult: {} = {};
  switchesAndPorts = [];
  vlanNumber: number;
  saveLabel = 'GUARDAR';
  action = 'Crear'
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.switchesAndPorts.push({
      dpid: null,
      ports: []
    })
  }

  openModal = (data: any = null, isEditing: boolean = false) => {
    this.isEditing(isEditing, data);
    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title', size: 'lg' }).result.then((result) => {
      this.formatPorts();
      this.closeResult = {
        vlan: parseInt(this.vlanNumber.toString()),
        switches: this.switchesAndPorts
      }
      this.onSaved.emit(this.closeResult);
    }, (reason) => {
      this.onClosed.emit(this.getDismissReason(reason));
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  addSwitch = () => {
    this.switchesAndPorts.push({
      dpid: null,
      ports: []
    })
  }

  removeSwitch = (index: number) => {
    if(this.switchesAndPorts.length !== 1) {
      this.switchesAndPorts.splice(index, 1);
    } else {
      this.switchesAndPorts[index] = {
        dpid: null,
        ports: []
      }
    }
  } 

  formatPorts = () => {
    if (this.switchesAndPorts) {
      this.switchesAndPorts = this.switchesAndPorts.map((value: any) => {
        return {
          dpid: value.dpid,
          ports: value.ports.map((p: any) => parseInt(p.value))
        }
      })
    }
  }

  isEditing = (isEditing: boolean, data: any) => {
    if (isEditing) {
      this.saveLabel = "ACTUALIZAR";
      this.action = 'Modificar';
      this.vlanNumber = data.vlan;
      this.switchesAndPorts = data.switches.map((s: any) => {
        return {
          dpid: s.dpid,
          ports: s.ports.map((p: any) => {
            return {
              display: p,
              value: p
            }
          })
        };
      });
    } else {
      this.action = 'Crear';
      this.saveLabel = "GUARDAR";
      this.vlanNumber = null;
      this.switchesAndPorts = [];
      this.addSwitch();
    }
  }

}
