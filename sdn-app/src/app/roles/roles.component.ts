import { Component, OnInit, ViewChild } from '@angular/core';
import { RolesService } from '../services/roles.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {AssignRoleDialogComponent} from '../assign-role-dialog/assign-role-dialog.component';
import { NotifyService } from '../services/notify.service';
import { LogsService } from '../services/logs.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  @ViewChild('assignRoleModal', {static : false}) assignRoleModal: AssignRoleDialogComponent;
  roles: any = [];
  rolesOptions = ['core', 'distribution', 'access'];
  constructor(private service: RolesService, private modalService: NgbModal, private notify: NotifyService) { }

  ngOnInit() {
    this.getRoles();
  }

  getRoles = () => {
    this.service.getRoles().subscribe((response: any) => {
      this.buildRoles(response);
    }, (error: Error) => {
      this.notify.error(error.message);
    });
  }

  setRoles = (roleObj: any) => {
    this.service.setRole(roleObj).subscribe(response => {
      this.notify.success('¡El rol se ha asignado exitósamente!');
      this.getRoles();
    }, (error: Error) => {
      this.notify.error(error.message);
    });
  }

  buildRoles = (roleObj: {}) => {
    if(roleObj) {
      this.roles = Object.keys(roleObj).map(role => {
        return {
          id: role,
          value: roleObj[role]
        }
      });
    }
  }

  openModal = () => {
    this.assignRoleModal.openModal();
  }

  onClosed = (reason: any) => {}

  onSaved = (result: any) => {
    if(result) {
      let obj = {};
      result.map((a: any) => {
        obj[a.switchId] = a.role;
      });
      this.setRoles(obj);
    }
  }
}
