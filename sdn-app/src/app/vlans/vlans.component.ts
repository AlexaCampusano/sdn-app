import { Component, OnInit, ViewChild } from '@angular/core';
import { VlansService } from '../services/vlans.service';
import { NotifyService } from '../services/notify.service';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { AddVlanDialogComponent } from '../add-vlan-dialog/add-vlan-dialog.component';

@Component({
  selector: 'app-vlans',
  templateUrl: './vlans.component.html',
  styleUrls: ['./vlans.component.css'],
  animations: [
    trigger('showHide',
      [
        transition(':enter', animate('500ms', style({
          opacity: 1
        }))),
        transition(':leave', animate('300ms', style({
          opacity: 0
        })))
      ]),
    trigger('openClose', 
      [
        state('close', style({
          transform: 'rotate(0deg)'
        })),
        state('open', style({
          transform: 'rotate(90deg)'
        })),
        transition('close=>open', animate('500ms')),
        transition('open=>close', animate('500ms'))
      ]
    )
  ]
})
export class VlansComponent implements OnInit {
  vlans = [];
  editing: boolean = false;
  @ViewChild('addVlanModal', {static : false}) addVlanModal: AddVlanDialogComponent;
  constructor(private service: VlansService, private notify: NotifyService) { }

  ngOnInit() {
    this.getVlans();
  }

  getVlans = () => {
    this.service.getVlans().subscribe(response => {
      this.vlans = response;
    }, (error: Error) => {
      this.notify.error(error.message);
    });
  }

  createVlans = (data: any) => {
    this.service.createVlans(data).subscribe(response => {
      this.notify.success('¡Vlan fue creada exitósamente!');
      this.getVlans();
    }, (error: Error) => {
      this.notify.error(error.message);
    });
  }

  deleteVlans = (vlanId: number) => {
    this.service.deleteVlan(vlanId).subscribe(response => {
      this.notify.success('¡Vlan fue eliminada exitósamente!');
      this.getVlans();
    }, (error: Error) => {
      this.notify.error(error.message);
    })
  }

  updateVlans = (data: any) => {
    this.service.updateVlans(data).subscribe(response => {
      this.notify.success('¡Vlan fue actualizada exitósamente!');
      this.getVlans();
    }, (error: Error) => {
      this.notify.error(error.message);
    })
  }

  toggleRowDetails = (index: number) => {
    this.vlans[index].displayDetails = !this.vlans[index].displayDetails;
    this.vlans[index].state = this.vlans[index].displayDetails ? 'visible' : 'hidden';
    this.vlans[index].arrowState = this.vlans[index].displayDetails ? 'open' : 'close';
  }

  openModal = (data: any = null) => {
    this.editing = data != null;
    this.addVlanModal.openModal(data, this.editing);
  }

  onClosed = (reason: any) => {}

  onSaved = (result: any) => {
    if(result) {
      if (!this.editing) {
        this.createVlans(result);
      } else {
        this.updateVlans(result);
      }
      
    }
  }

}
