import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RolesComponent } from './roles/roles.component';
import { VlansComponent } from './vlans/vlans.component';
import { LogsComponent } from './logs/logs.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { FooterComponent } from './footer/footer.component';
import { AssignRoleDialogComponent } from './assign-role-dialog/assign-role-dialog.component';
import { AddVlanDialogComponent } from './add-vlan-dialog/add-vlan-dialog.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from 'src/routes';
import { ContainerComponent } from './container/container.component';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TagInputModule } from 'ngx-chips';
import { LogsService } from './services/logs.service';
import { RolesService } from './services/roles.service';
import { VlansService } from './services/vlans.service';
import { NotifyService } from './services/notify.service';

@NgModule({
  declarations: [
    AppComponent,
    RolesComponent,
    VlansComponent,
    LogsComponent,
    SideBarComponent,
    TopBarComponent,
    FooterComponent,
    AssignRoleDialogComponent,
    AddVlanDialogComponent,
    ContainerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
    TagInputModule
  ],
  providers: [LogsService, RolesService, VlansService, NotifyService],
  bootstrap: [AppComponent],
  entryComponents: [
    AssignRoleDialogComponent,
    AddVlanDialogComponent
  ]
})
export class AppModule { }
