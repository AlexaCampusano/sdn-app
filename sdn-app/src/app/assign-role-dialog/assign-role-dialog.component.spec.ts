import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRoleDialogComponent } from './assign-role-dialog.component';

describe('AssignRoleDialogComponent', () => {
  let component: AssignRoleDialogComponent;
  let fixture: ComponentFixture<AssignRoleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignRoleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignRoleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
