import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-assign-role-dialog',
  templateUrl: './assign-role-dialog.component.html',
  styleUrls: ['./assign-role-dialog.component.css']
})
export class AssignRoleDialogComponent implements OnInit {
  @Input() roles: [];
  @ViewChild('content', { static: false}) content: any;
  @Output() onClosed: EventEmitter<any> = new EventEmitter();
  @Output() onSaved: EventEmitter<any> = new EventEmitter();
  closeResult = [];
  switchId: string = "";
  constructor(private modalService: NgbModal) { }

  ngOnInit() {
    this.closeResult.push({
      switchId: null,
      role: 'Core'
    });
  }

  openModal() {
    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.onSaved.emit(this.closeResult);
    }, (reason) => {
      this.onClosed.emit(this.getDismissReason(reason));
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  addSwitch = () => {
    this.closeResult.push({
      switchId: null,
      role: null
    });
  }

  removeSwitch = (index: number) => {
    if(this.closeResult.length !== 1) {
      this.closeResult.splice(index, 1);
    } else {
      this.closeResult[index] = {
        dpid: null,
        ports: []
      }
    }
  }

}
