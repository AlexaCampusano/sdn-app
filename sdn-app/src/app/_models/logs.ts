export class Log {
    date: string;
    createdBy: string;
    description: string;
    dataSent: string;
    apiRoute: string;
    displayDetails: boolean;
    state: string;
    arrowState: string;

    constructor(createdBy: string, description: string, dataSent: string, apiRoute: string) {
        this.createdBy = createdBy;
        this.description = description;
        this.date = new Date().toLocaleString();
        this.dataSent = dataSent;
        this.apiRoute = apiRoute;
    }
}