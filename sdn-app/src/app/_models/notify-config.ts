export class NotifyConfig {
    title: string;
    text: string;
    type: string;
    icon: string;
    timeout: number = 5000;
    callback: () => {}

    constructor(title: string, text: string, type: string, icon: string, callback: any, timeout: number = 5000) {
        this.title = title;
        this.text = text;
        this.type = type;
        this.icon = icon;
        this.timeout = timeout;
        this.callback = callback;
    }
}