import { Component, OnInit } from '@angular/core';
import { LogsService } from '../services/logs.service';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { Log } from '../_models/logs';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css'],
  animations: [
    trigger('showHide',
      [
        transition(':enter', animate('500ms', style({
          opacity: 1
        }))),
        transition(':leave', animate('300ms', style({
          opacity: 0
        })))
      ]),
    trigger('openClose', 
      [
        state('close', style({
          transform: 'rotate(0deg)'
        })),
        state('open', style({
          transform: 'rotate(90deg)'
        })),
        transition('close=>open', animate('500ms')),
        transition('open=>close', animate('500ms'))
      ]
    )
  ]
})
export class LogsComponent implements OnInit {
  logs: Log[] = [];
  constructor(private service: LogsService) { }

  ngOnInit() {
    this.getLogs()
  }

  getLogs = () => {
    this.service.getLogs().subscribe((response: any) => {
      if (response) {
        this.logs = JSON.parse(response.data);
      }
    })
  }

  toggleRowDetails = (index: number) => {
    this.logs[index].displayDetails = !this.logs[index].displayDetails;
    this.logs[index].state = this.logs[index].displayDetails ? 'visible' : 'hidden';
    this.logs[index].arrowState = this.logs[index].displayDetails ? 'open' : 'close';
  }

}
